
from flask import Flask, render_template, url_for, flash, redirect

from forms import RegistrationForm, LoginForm

import json

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'

rifat = True

posts = [
    {
        'name': 'ilk gunu gibi temiz',
        'title': '3 oda',
        'content': '1 salon',
        'date_posted': 'April 20, 2018'
    }
]

with open('posts.txt','w' ) as outfile:
    json.dump(posts,outfile)

user_list = {"admin":{"password":"admin",'properties': [{"property_id":"1", "name":"ev", "numberOfBedrooms":1, "numberOfRooms":3, "sqmeter":100}], "bookings": []},"rifat":{"password":"111", "properties":[{"property_id":"2", "name":"ev", "numberOfBedrooms":1, "numberOfRooms":2, "sqmeter":120}]},"esra":{"password":"222", "properties":[]}}
with open('user_list.txt','w' ) as outfile:
    json.dump(user_list,outfile)

@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    user_list.update({form.username.data: {"password": form.password.data, "properties": [], "bookings": []}})
    with open('user_list.txt', 'w') as outfile:
        json.dump(user_list, outfile)
    if form.validate_on_submit():
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.password.data == user_list.get(form.username.data).get("password"):
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

# @app.route('/properties', methods=['GET'])
# def properties():
#     ...
#
#
#
# @app.route('/property', methods=['POST'])
# def property():
#     # request
#     ...
#
#
# @app.route('/property/create', methods=['POST'])
# def property_create():
    # request

#
#
# @app.route('/property/update', methods=['POST'])
# def property_update():
#     # request
#     ...
#
#
# @app.route('/property/delete', methods=['DELETE'])
# def property_delete():
#     # request
#     ...


if __name__ == '__main__':
    app.run(debug=True)